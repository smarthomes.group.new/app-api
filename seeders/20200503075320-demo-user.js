'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert("Users", [
      {
        firstName: "Hoang",
        lastName: "Huy",
        email: "hoanguet1992@gmail.com",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        firstName: "Tuan",
        lastName: "Huy",
        email: "hoanguet1991@gmail.com",
        createdAt: new Date(),
        updatedAt: new Date()
      }
    ], {})
  },

  down: (queryInterface, Sequelize) => {
    /*
      Add reverting commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.bulkDelete('People', null, {});
    */
    return queryInterface.bulkDelete('Users', null, {});
  }
};
