var express = require('express');
var router = express.Router();
const db = require("../models/index")
const User = db.User

/* GET users listing. */
router.get('/view/:userId', function(req, res, next) {
  const params = req && req.params
  User.findOne({
    where: {
      id: params && params.userId
    }
  }).then(result => {
    res.status(200).json({user: result})
  })
});

module.exports = router;
